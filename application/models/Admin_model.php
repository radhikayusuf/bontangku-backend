<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model
{
	public function ambilSatuData($where)
	{
		return $this->db->get_where('admin', $where)->row_array();
	}

	public function ambilSemuaData()
	{
		return $this->db->get('admin')->result();
	}

	public function tambahData($data)
	{
		return $this->db->insert('admin', $data);
	}

	public function ubahData($data, $where)
	{
		return $this->db->update('admin', $data, $where);
	}

	public function deleteData($where)
	{
		return $this->db->delete('admin', $where);
	}
}
