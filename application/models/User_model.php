<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
	public function ambilSatuData($where)
	{
		return $this->db->get_where('user', $where)->row_array();
	}

	public function ambilSemuaData()
	{
		return $this->db->get('user')->result();
	}

	public function tambahData($data)
	{
		return $this->db->insert('user', $data);
	}

	public function ubahData($data, $where)
	{
		return $this->db->update('user', $data, $where);
	}

	public function deleteData($where)
	{
		return $this->db->delete('user', $where);
	}
}
