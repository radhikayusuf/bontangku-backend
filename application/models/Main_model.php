<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main_model extends CI_Model
{
	public function ambilSatuData($table, $where)
	{
		return $this->db->get_where($table, $where)->row_array();
	}

	public function ambilSemuaData($table)
	{
		return $this->db->get($table)->result();
	}

	public function tambahData($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	public function ubahData($table, $data, $where)
	{
		return $this->db->update($table, $data, $where);
	}

	public function deleteData($table, $where)
	{
		return $this->db->delete($table, $where);
	}
}
