<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lokasi_model extends CI_Model
{
	public function ambilSatuData($where)
	{
		return $this->db->get_where('lokasi', $where)->row_array();
	}

	public function ambilSemuaData()
	{
		return $this->db->get('lokasi')->result();
	}

	public function tambahData($data)
	{
		return $this->db->insert('lokasi', $data);
	}

	public function ubahData($data, $where)
	{
		return $this->db->update('lokasi', $data, $where);
	}

	public function deleteData($where)
	{
		return $this->db->delete('lokasi', $where);
	}
}
