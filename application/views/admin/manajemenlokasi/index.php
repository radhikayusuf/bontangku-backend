<!-- flashdata sweetalert -->
<div class="flash-data" data-flashdata="<?= $this->session->flashdata('message') ?>" data-title="<?= $this->session->flashdata('title') ?>" data-type="<?= $this->session->flashdata('type') ?>"></div>
<?php $this->session->unset_userdata(['message','title','type']); ?>

<div class="col-12 mx-auto">

	<a href="<?= base_url('admin/manajemenlokasi/tambah') ?>" class="btn btn-primary mt-3 mb-3 rounded">
		<i class="fas fa-plus"></i> Tambah Lokasi
	</a>

	<div class="card">
		<div class="card-header text-white" style="background-color: #625FFF!important;"><strong>Manajemen Lokasi</strong></div>
		<div class="card-body">

			<div class="table-responsive">
				<table class="table table-striped text-center" style="width:100%" id="datalokasi">
					<thead>
						<tr>
							<th>#</th>
							<th>Nama Lokasi</th>
							<th>Deskripsi</th>
							<th>Latitude</th>
							<th>Longtitude</th>
							<th>Type</th>
							<th>URL</th>
							<th></th>
						</tr> 
					</thead>
					<tbody>
						<?php $i = 1;
						foreach ($lokasi as $getdata) : ?>
							<tr id="<?= $getdata->id_lokasi ?>">
								<td><?= $i++ ?></td>
								<td><?= $getdata->nama_lokasi ?></td>
								<td><?= $getdata->deskripsi ?></td>
								<td><?= $getdata->latitude ?></td>
								<td><?= $getdata->longtitude ?></td>
								<td><?= $getdata->type ?></td>
								<td><?= $getdata->url ?></td>
								<td>
									<a href="<?= base_url('admin/manajemenlokasi/ubah/' . $getdata->id_lokasi) ?>" class="btn btn-sm btn-primary rounded"><i class="fas fa-edit"></i></a>
									<button type="button" data-id="<?= $getdata->id_lokasi ?>" data-nama="LokasiIni" class="btn btn-sm btn-danger remove rounded"><i class="fas fa-trash"></i></button>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
