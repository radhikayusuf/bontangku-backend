<div class="col-6 mx-auto my-5">
	<div class="card card-tambah">
		<div class="card-body">
			<?= form_open('admin/manajemenlokasi/prosestambahlokasi', ['data-toggle' => 'validator', 'role' => 'form']) ?>

			<div class="form-group has-feedback">
				<label for="nama_lokasi">Nama Lokasi <span class="text-danger"><strong>*</strong></span></label>
				<input type="text" class="form-control" id="nama_lokasi" name="nama_lokasi" data-required-error="Nama Lokasi tidak boleh kosong" value="<?= set_value('nama_lokasi') ?>" required>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<span class="help-block with-errors"></span>
				<?php echo form_error('nama_lokasi', '<div class="text-danger">', '</div>'); ?>
			</div>

			<div class="form-group has-feedback">
				<label for="deskripsi">Deskripsi <span class="text-danger"><strong>*</strong></span></label>
				<textarea class="form-control" id="deskripsi" name="deskripsi" data-required-error="Deskripsi tidak boleh kosong" required rows="3" cols="5"><?= set_value('deskripsi') ?></textarea>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<span class="help-block with-errors"></span>
				<?php echo form_error('deskripsi', '<div class="text-danger">', '</div>'); ?>
			</div>

			<div class="form-group has-feedback">
				<label for="latitude">Latitude <span class="text-danger"><strong>*</strong></span></label>
				<input type="text" class="form-control" id="latitude" name="latitude" data-required-error="Latitude tidak boleh kosong" value="<?= set_value('latitude') ?>" required>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<span class="help-block with-errors"></span>
				<?php echo form_error('latitude', '<div class="text-danger">', '</div>'); ?>
			</div>

			<div class="form-group has-feedback">
				<label for="longtitude">Longtitude <span class="text-danger"><strong>*</strong></span></label>
				<input type="text" class="form-control" id="longtitude" name="longtitude" data-required-error="Longtitude tidak boleh kosong" value="<?= set_value('longtitude') ?>" required>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<span class="help-block with-errors"></span>
				<?php echo form_error('longtitude', '<div class="text-danger">', '</div>'); ?>
			</div>

			<div class="form-group has-feedback">
				<label for="type">Type <span class="text-danger"><strong>*</strong></span></label>
				<input type="text" class="form-control" id="type" name="type" data-required-error="Type tidak boleh kosong" value="<?= set_value('type') ?>" required>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<span class="help-block with-errors"></span>
				<?php echo form_error('type', '<div class="text-danger">', '</div>'); ?>
			</div>

			<div class="form-group has-feedback">
				<label for="url">URL <span class="text-danger"><strong>*</strong></span></label>
				<input type="text" class="form-control" id="url" name="url" data-required-error="URL tidak boleh kosong" value="<?= set_value('url') ?>" required>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<span class="help-block with-errors"></span>
				<?php echo form_error('url', '<div class="text-danger">', '</div>'); ?>
			</div>


			<div class="form-group text-right mt-3">
				<button type="submit" class="btn btn-primary rounded">Tambah</button>
				<a href="<?= base_url('admin/manajemenadmin/index') ?>" class="btn btn-primary rounded">Kembali</a>
			</div>
			<?= form_close() ?>
		</div>
	</div>
</div>
