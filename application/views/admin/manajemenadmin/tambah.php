<div class="col-6 mx-auto my-5">
	<div class="card card-tambah">
		<div class="card-body">
			<?= form_open('admin/manajemenadmin/prosestambahadmin', ['data-toggle' => 'validator', 'role' => 'form']) ?>

			<div class="form-group has-feedback">
				<label for="nama">Nama <span class="text-danger"><strong>*</strong></span></label>
				<input type="text" class="form-control" id="nama" name="nama" data-required-error="Nama tidak boleh kosong" value="<?= set_value('nama') ?>" required>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<span class="help-block with-errors"></span>
				<?php echo form_error('nama', '<div class="text-danger">', '</div>'); ?>
			</div>

			<div class="form-group has-feedback">
				<label for="email">Email <span class="text-danger"><strong>*</strong></span></label>
				<input type="email" class="form-control" id="email" name="email" data-required-error="Email tidak boleh kosong" value="<?= set_value('email') ?>" required>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<span class="help-block with-errors"></span>
				<?php echo form_error('email', '<div class="text-danger">', '</div>'); ?>
			</div>

			<div class="form-group has-feedback">
				<label for="password">Password <span class="text-danger"><strong>*</strong></span></label>
				<input type="password" class="form-control" id="password" name="password" data-required-error="Password tidak boleh kosong" value="<?= set_value('password') ?>" required>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<span class="help-block with-errors"></span>
				<?php echo form_error('password', '<div class="text-danger">', '</div>'); ?>
			</div>

			<div class="form-group has-feedback">
				<label for="ulangipassword">Ulangi Password <span class="text-danger"><strong>*</strong></span></label>
				<input type="password" class="form-control" id="ulangipassword" name="ulangipassword" data-match="#password" data-match-error="Oppss, Password Tidak Sama." data-required-error="Ulangi Password tidak boleh kosong" value="<?= set_value('ulangipassword') ?>" required>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<span class="help-block with-errors"></span>
				<?php echo form_error('ulangipassword', '<div class="text-danger">', '</div>'); ?>
			</div>

			<div class="form-group text-right mt-3">
				<button type="submit" class="btn btn-primary rounded">Tambah</button>
				<a href="<?= base_url('admin/manajemenadmin/index') ?>" class="btn btn-primary rounded">Kembali</a>
			</div>
			<?= form_close() ?>
		</div>
	</div>
</div>
