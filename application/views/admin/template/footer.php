<footer class="app-footer">
	<div>
		<span>&copy; <?= date('Y') ?> Copyright BONTANG APP </a> </span>
	</div>
	<div class="ml-auto">
		<span>Developed by <a href="https://about.me/moriram" target="_blank">Rifqi Ramdhani</a></span>
		<a href="#"></a>
	</div>
</footer>

<script src="<?= base_url('assets/vendors/jquery/js/jquery.min.js') ?>">
</script>
<script src="<?= base_url('assets/vendors/bootstrap/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') ?>"></script>
<script src="<?= base_url('assets/vendors/pace-progress/js/pace.min.js') ?>"></script>
<script src="<?= base_url('assets/vendors/@coreui/coreui/js/coreui.min.js') ?>"></script>
<!-- form validation -->
<script src="<?= base_url('assets/node_modules/bootstrap-validator/dist/validator.min.js') ?>"></script>
<!-- datatables -->
<script src="<?= base_url('assets/vendors/datatables/datatables.min.js') ?>"></script>
<!-- custom js -->
<script src="<?= base_url('assets/js/custom.js') ?>" type="text/javascript" charset="utf-8"></script>
<!-- fontawesome -->
<script src="<?= base_url('assets/js/all.min.js') ?>" type="text/javascript" charset="utf-8"></script>
<!-- sweetaler 2 -->
<script src="<?= base_url('assets/js/sweetalert2.all.min.js') ?>"></script>
<!-- custom file input -->
<script src="<?= base_url('assets/node_modules/bs-custom-file-input/dist/bs-custom-file-input.min.js') ?>"></script>


<script>
	// Setup datatables 
	$(document).ready(function() {
		$("#datapembatalan").dataTable({
			dom: 'Bfrtip',
			buttons: [
				'excel'
			]
		})

		$('#file-upload1').change(function() {
			var file = $(this)[0].files[0].name;
			$("#filename1").text(file)
		});

		$('#file-upload2').change(function() {
			var file = $(this)[0].files[0].name;
			$("#filename2").text(file)
		});


		//init custom file input
		bsCustomFileInput.init()

		// sweetalert
		const flashdata = $('.flash-data').data('flashdata');
		const title = $('.flash-data').data('title');
		const type = $('.flash-data').data('type');

		if (flashdata) {
			Swal.fire({
				title: title,
				text: flashdata,
				icon: type
			})
		}

		// Setup datatables
		$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};

		//----------------------footer content-----------------------

		// dataadmin
		$("#dataadmin").dataTable()
		$("#dataadmin").on('click', '.remove', function() {
			var id = $(this).data('id');
			var nama = $(this).data('nama');
			hapusdata('Manajemen Admin', "<?= base_url('admin/manajemenadmin/prosesHapusAdmin/') ?>", id, nama)
		})

		// datauser
		$("#datauser").dataTable()
		$("#datauser").on('click', '.remove', function() {
			var id = $(this).data('id');
			var nama = $(this).data('nama');
			hapusdata('Manajemen User', "<?= base_url('admin/manajemenuser/proseshapususer/') ?>", id, nama)
		})

		// datalokasi
		$("#datalokasi").dataTable()
		$("#datalokasi").on('click', '.remove', function() {
			var id = $(this).data('id');
			var nama = $(this).data('nama');
			hapusdata('Manajemen Lokasi', "<?= base_url('admin/manajemenlokasi/proseshapuslokasi/') ?>", id, nama)
		})

		// datalayanan
		$("#datalayanan").dataTable()
		


		$("#datamahasiswa").on('click', '.datadisetujui', function() {
			var id = $(this).data('id');
			var nama = $(this).data('nama');
			var link = '<?= base_url('admin/mahasiswa/dataDisetujui/') ?>' + id
			var title = 'Akan Menyetujui Data ' + nama + ' ?'
			swalhref(link, title)
		})

		$("#datamahasiswa").on('click', '.dataditolak', function() {
			var id = $(this).data('id');
			var nama = $(this).data('nama');
			var link = '<?= base_url('admin/mahasiswa/dataDitolak/') ?>' + id
			var title = 'Tidak Akan Menyetujui Data ' + nama + ' ?'
			swalhref(link, title)
		})

		$("#btn-logout").on('click', function(){
			var link = '<?= base_url('auth/logout') ?>'
			Swal.fire({
				title: 'Anda akan logout',
				text: " ",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#1C42CA',
				confirmButtonText: 'Lanjut',
				cancelButtonText: 'Tidak'
			}).then((result) => {
				if (result.value) {
					window.location.href = link
				}
			})
		})


		// table export
		var tableexport = $('#tableexport').DataTable({
			lengthChange: false,
			buttons: ['excel', 'pdf', 'print']
		});

		tableexport.buttons().container().appendTo('#tableexport_wrapper .col-md-6:eq(0)');

	});
</script>
</body>

</html>
