<header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="<?= base_url() ?>">
        <img class="navbar-brand-full" src="<?= base_url('assets/img/logo/bontang.png') ?>" width="40" height="50" alt="Logo Kota Bontang">
        <img class="navbar-brand-minimized" src="<?= base_url('assets/img/logo/bontang.png') ?>" width="30" height="40" alt="Logo Kota Bontang">
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <!-- navbar kanan -->
    <ul class="nav navbar-nav ml-auto">
        
    </ul>
</header>
