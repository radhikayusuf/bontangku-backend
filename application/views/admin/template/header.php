<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<meta name="description" content="Admin of ASIA-PACIFIC URBAN DESIGNERS">
	<meta name="author" content="Rifqi Ramdhani">
	<meta name="keyword" content="Apud">
	<title><?= $title ?></title>
	<!-- favicon -->
	<link rel="apple-touch-icon" sizes="57x57" href="<?= base_url('assets/img/favicon/') ?>apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= base_url('assets/img/favicon/') ?>apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= base_url('assets/img/favicon/') ?>apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('assets/img/favicon/') ?>apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= base_url('assets/img/favicon/') ?>apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('assets/img/favicon/') ?>apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= base_url('assets/img/favicon/') ?>apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('assets/img/favicon/') ?>apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/img/favicon/') ?>apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="<?= base_url('assets/img/favicon/') ?>/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('assets/img/favicon/') ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= base_url('assets/img/favicon/') ?>/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/img/favicon/') ?>/favicon-16x16.png">
	<link rel="manifest" href="<?= base_url('assets/img/favicon/') ?>/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?= base_url('assets/img/favicon/') ?>/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<!-- datatables -->
	<link rel="stylesheet" href="<?= base_url('assets/vendors/datatables/datatables.min.css') ?>">
	<!-- font -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Dosis:400,500,600,700" />
	<!-- custom -->
	<link rel="stylesheet" href="<?= base_url('assets/css/custom.css') ?>">
	<!-- Main styles for this application-->
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendors/pace-progress/css/pace.min.css') ?>" rel="stylesheet">

	<style>
		.ui-autocomplete-loading {
			background: white url(<?= base_url('assets/img/ui-anim_basic_16x16.gif') ?>) right center no-repeat;
		}
	</style>

</head>
