<!-- flashdata sweetalert -->
<div class="flash-data" data-flashdata="<?= $this->session->flashdata('message') ?>" data-title="<?= $this->session->flashdata('title') ?>" data-type="<?= $this->session->flashdata('type') ?>"></div>
<?php $this->session->unset_userdata(['message','title','type']); ?>

<div class="col-8 mx-auto">

	<div class="card">
		<div class="card-header text-white" style="background-color: #625FFF!important;"><strong>Layanan 112</strong></div>
		<div class="card-body">

			<div class="table-responsive">
				<table class="table table-striped text-center" style="width:100%" id="datalayanan">
					<thead>
						<tr>
							<th>#</th>
							<th>Id User</th>
							<th>No HP</th>
							<th>Latitude</th>
							<th>Longtitude</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1;
						foreach ($layanan as $getdata) : ?>
							<tr id="<?= $getdata->id_layanan ?>">
								<td><?= $i++ ?></td>
								<td><?= $getdata->id_user ?></td>
								<td><?= $getdata->no_hp ?></td>
								<td><?= $getdata->latitude ?></td>
								<td><?= $getdata->longtitude ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
