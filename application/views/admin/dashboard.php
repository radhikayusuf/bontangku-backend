<!-- flashdata sweetalert -->
<div class="flash-data" data-flashdata="<?= $this->session->flashdata('message') ?>" data-title="<?= $this->session->flashdata('title') ?>" data-type="<?= $this->session->flashdata('type') ?>"></div>
<?php $this->session->unset_userdata(['message','title','type']); ?>


<div class="container-fluid">
	<div class="animated fadeIn">
		<div class="row ">

			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="row">


						<div class="col-lg-6 col-sm-6 col-xs-12  mx-auto">
							<div class="info-box bg-young-purple hover-expand-effect">
								<div class="icon">
									<i class="fas fa-user-friends"></i>
								</div>
								<div class="content">
									<div class="text text-white">Total Pengguna</div>
									<div class="number count-to  text-white">11</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-sm-6 col-xs-12  mx-auto">
							<div class="info-box bg-young-purple hover-expand-effect">
								<div class="icon">
									<i class="fas fa-user-plus"></i>
								</div>
								<div class="content">
									<div class="text text-white">Pengguna Baru</div>
									<div class="number count-to text-white">11</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
