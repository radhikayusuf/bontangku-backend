<!-- flashdata sweetalert -->
<div class="flash-data" data-flashdata="<?= $this->session->flashdata('message') ?>" data-title="<?= $this->session->flashdata('title') ?>" data-type="<?= $this->session->flashdata('type') ?>"></div>
<?php $this->session->unset_userdata(['message','title','type']); ?>

<div class="col-8 mx-auto">

	<a href="<?= base_url('admin/manajemenuser/tambah') ?>" class="btn btn-primary mt-3 mb-3 rounded">
		<i class="fas fa-plus"></i> Tambah User
	</a>

	<div class="card">
		<div class="card-header text-white" style="background-color: #625FFF!important;"><strong>Manajemen User</strong></div>
		<div class="card-body">

			<div class="table-responsive">
				<table class="table table-striped text-center" style="width:100%" id="datauser">
					<thead>
						<tr>
							<th>#</th>
							<th>Nama</th>
							<th>Email</th>
							<th>No HP</th>
							<th></th> 
						</tr>
					</thead>
					<tbody>
						<?php $i = 1;
						foreach ($user as $getdata) : ?>
							<tr id="<?= $getdata->id_user ?>">
								<td><?= $i++ ?></td>
								<td><?= $getdata->nama ?></td>
								<td><?= $getdata->email ?></td>
								<td><?= $getdata->no_hp ?></td>
								<td>
									<a href="<?= base_url('admin/manajemenuser/ubah/' . $getdata->email) ?>" class="btn btn-sm btn-primary rounded"><i class="fas fa-edit"></i></a>
									<button type="button" data-id="<?= $getdata->id_user ?>" data-nama="<?= $getdata->nama ?>" class="btn btn-sm btn-danger remove rounded"><i class="fas fa-trash"></i></button>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
