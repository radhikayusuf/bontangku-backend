<div class="sidebar">
	<nav class="sidebar-nav">
		<ul class="nav">
			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('admin/dashboard') ?>">
					<i class="nav-icon fas fa-tachometer-alt"></i> Dashboard
				</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('admin/manajemenadmin') ?>">
					<i class="nav-icon fas fa-user-friends"></i> Manajemen Admin
				</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('admin/manajemenuser') ?>">
					<i class="nav-icon fas fa-user-friends"></i> Manajemen User
				</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('admin/manajemenlokasi') ?>">
					<i class="nav-icon fas fa-map-marker-alt"></i> Manajemen Lokasi
				</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="<?= base_url('admin/layanan') ?>">
					<i class="nav-icon fas fa-phone-volume"></i> Layanan 112
				</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="#" id="btn-logout">
					<i class="nav-icon fa fa-sign-out-alt"></i> Logout
				</a>
			</li>
		</ul>
	</nav>
</div>
