<!-- flashdata sweetalert -->
<div class="flash-data" data-flashdata="<?= $this->session->flashdata('message') ?>" data-title="<?= $this->session->flashdata('title') ?>" data-type="<?= $this->session->flashdata('type') ?>"></div>
<?php $this->session->unset_userdata(['message','title','type']); ?>

<!-- login -->
<div class="col-md-6" id="form-login">
	<div class="card mx-4">
		<?= form_open('auth/proseslogin', ['data-toggle' => 'validator', 'role' => 'form']) ?>

		<div class="card-body p-4">
			<div class="text-center">
				<div class="row d-flex align-items-center">
					<div class="col-md-4 text-center">
						<img src="<?= base_url('assets/img/logo/bontang.png') ?>" alt="" width="50%">
					</div>
					<div class="col-md-8 text-left">
						<h3 class="font-weight-bold">
							Bontang SuperApps
						</h3>
					</div>
				</div>

			</div>
			<div class="form-group has-feedback mt-4">
				<label for="email">Email</label>
				<input type="email" name="email" placeholder="mail@mail.com" class="form-control" data-required-error="Email tidak boleh kosong" required>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<div class="help-block with-errors"></div>
			</div>
			<div class="form-group has-feedback">
				<label for="password">Password</label>
				<input type="password" name="password" placeholder="Password" class="form-control" data-required-error="Password tidak boleh kosong" required>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<div class="help-block with-errors"></div>
			</div>

			<button class="btn btn-block btn-primary" type="submit">LOGIN</button>

			<div class="row">
				<div class="col-12 text-center mt-3">
					<button class="btn btn-link px-0 btn-forgot-password" type="button">Lupa password?</button>
				</div>
			</div>
		</div>
		<?= form_close() ?>
	</div>
</div>

<!-- forgot password -->
<div class="col-md-7" id="form-forgot-password" style="display: none">
	<div class="card mx-4">
		<?= form_open('auth/proseslupapassword', ['data-toggle' => 'validator', 'role' => 'form']) ?>

		<div class="card-body p-4">
			<div class="header mb-3">
				<button type="button" class="close float-right btn-login" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="text-center">
				<h3>
					<i class="fa fa-lock fa-4x mb-4"></i>
				</h3>
				<h2>LUPA PASSWORD</h2>
				<p>Anda bisa mengubah password disini.</p>
			</div>
			<div class="form-group has-feedback">
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text"><i class="fas fa-envelope"></i></span>
					</div>
					<input class="form-control" type="email" name="email" value="<?= set_value('email') ?>" placeholder="mail@mail.com" data-required-error="Email tidak boleh kosong" required>
				</div>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<div class="help-block with-errors"></div>
				<?= form_error('email', '<div class="text-danger">', '</div>') ?>
			</div>

			<button class="btn btn-block btn-primary" type="submit">SUBMIT</button>
		</div>
		<?= form_close() ?>
	</div>
</div>
