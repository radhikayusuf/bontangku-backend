<!DOCTYPE html>
<html lang="en">

<head>
	<base href="./">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<title>Portal - BONTANG SUPERAPPS</title>
	<!-- favicon -->
	<link rel="apple-touch-icon" sizes="57x57" href="<?= base_url('assets/img/favicon/') ?>apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= base_url('assets/img/favicon/') ?>apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= base_url('assets/img/favicon/') ?>apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('assets/img/favicon/') ?>apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= base_url('assets/img/favicon/') ?>apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('assets/img/favicon/') ?>apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= base_url('assets/img/favicon/') ?>apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('assets/img/favicon/') ?>apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/img/favicon/') ?>apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="<?= base_url('assets/img/favicon/') ?>/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('assets/img/favicon/') ?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= base_url('assets/img/favicon/') ?>/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/img/favicon/') ?>/favicon-16x16.png">
	<link rel="manifest" href="<?= base_url('assets/img/favicon/') ?>/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?= base_url('assets/img/favicon/') ?>/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<!-- line icons -->
	<link href="<?= base_url('assets/vendors/simple-line-icons/css/simple-line-icons.css') ?>" rel="stylesheet">
	<!-- style core ui -->
	<link rel="stylesheet" href="<?= base_url('assets/css/custom.css') ?>">

	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendors/pace-progress/css/pace.min.css') ?>" rel="stylesheet">
</head>

<body class="app flex-row align-items-center">
	<div class="container">
		<div class="row justify-content-center">
			<?php echo $contents; ?>
		</div>
	</div>

	<script src="<?= base_url('assets/vendors/jquery/js/jquery.min.js') ?>"></script>
	<script src="<?= base_url('assets/vendors/bootstrap/js/bootstrap.min.js') ?>"></script>
	<script src="<?= base_url('assets/vendors/pace-progress/js/pace.min.js') ?>"></script>
	<script src="<?= base_url('assets/vendors/@coreui/coreui/js/coreui.min.js') ?>"></script>
	<!-- sweetaler 2 -->
	<script src="<?= base_url('assets/js/sweetalert2.all.min.js') ?>"></script>
	<!-- form validation -->
	<script src="<?php echo base_url(); ?>assets/node_modules/bootstrap-validator/dist/validator.min.js"></script>
	<!-- fontawesome -->
	<script src="<?= base_url('assets/js/all.min.js') ?>" type="text/javascript" charset="utf-8"></script>
	<script>
		$(document).ready(function() {
			// sweetalert
			const flashdata = $('.flash-data').data('flashdata');
			const title = $('.flash-data').data('title');
			const type = $('.flash-data').data('type');

			if (flashdata) {
				Swal.fire({
					title: title,
					text: flashdata,
					icon: type
				})
			}

			$('.btn-register').click(function() {
				$('#form-forgot-password').hide()
				$('#form-login').hide()
				$('#form-register').show()
			})

			$('.btn-login').click(function() {
				$('#form-register').hide()
				$('#form-forgot-password').hide()
				$('#form-login').show()
			})

			$('.btn-forgot-password').click(function() {
				$('#form-register').hide()
				$('#form-login').hide()
				$('#form-forgot-password').show()
			})

			// setTimeout(function() {
			//     $(".btn-register").click();
			// }, 1000);
			<?= isset($script) ? $script : ''; ?>
		})
	</script>


	<!-- script menampilkan form regist -->


</body>

</html>
