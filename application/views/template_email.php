<div style="width:80%;margin:0 auto;font-family: sans-serif;">

    <div style="background: radial-gradient( circle, rgba(198, 158, 70, 1) 100%, rgba(251, 200, 87, 1) 100% );color:white;text-align:center;padding:3px;margin-top:10px;margin-bottom:40px">
        <h2>INFORMASI</h2>
    </div>

    <div style="margin-bottom:10px;font-family: sans-serif;padding-left: 40px">
        <p>
            <?= $pesan ?>
        </p>
    </div>

    <div style="font-family: sans-serif;margin-top: 40px">
        <table style="background-color:#212121;color:#ddd;font-size:15px;width:100%;" bgcolor="#212121" cellpadding="0" cellspacing="0" border="0">
            <tbody>
                <tr>
                    <td style="border-left:1px solid #212121;border-right:1px solid #212121" height="40"></td>
                </tr>
                <tr>
                    <td style="text-align:center;border-left:1px solid #212121;border-right:1px solid #212121;font-family:'Segoe UI',Tahoma,Geneva,Verdana,sans-serif">
                        <span>Do not reply</span>
                        <br>
                        <span>©2020 King Volate | Luxury Car Rental Specialist</span>
                    </td>
                </tr>
                <tr>
                    <td style="border-left:1px solid #212121;border-right:1px solid #212121;text-align:center">
                        <a href="" target="_blank"><img style="margin-right:15px" height="25" width="25" src="https://ci3.googleusercontent.com/proxy/DccKFX_5Sb12iyeuUmen2c7WmI6qJ8n9-2aifO3ZVMch6-S9K9kqQqQIwLQvUIwoldDR35oPJYTU1Mdz1OhdGWMkRYYTMeX-ye10oFWkcqA=s0-d-e1-ft#https://www.freelogodesign.org/Content/img/email-facebook.png" alt="Facebook" class="CToWUd"></a>
                        <a href="" target="_blank"><img style="margin-right:15px" height="25" width="25" src="https://ci3.googleusercontent.com/proxy/qjR6YFoi9oZhXK3mjVQUdYtv6C9U7TkN0vg-Jc8jaxittTyFkB82EI--KFeuTTJEJb8MDJoGDCOT3SAkAosWCbsn8f8Rvm-8lbjiI0L4lcWg=s0-d-e1-ft#https://www.freelogodesign.org/Content/img/email-pinterest.png" alt="Pinterest" class="CToWUd"></a>
                        <a href="" target="_blank"><img height="25" width="25" src="https://ci3.googleusercontent.com/proxy/nONnYrbTrd7NitxPG4-RJ_9EHCX9KOZPwamNSDjmjxcP6oyJoktPsx3ve_z0mge7Rf_qHmVqKI0Ze2otztaL-5nJJaYxP1yuVfIzOsq_2AjH=s0-d-e1-ft#https://www.freelogodesign.org/Content/img/email-instagram.png" alt="Intagram" class="CToWUd"></a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>