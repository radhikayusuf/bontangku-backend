<tbody>
	<tr>
		<td style="padding:0;width:26px"></td>
		<td style="padding:0">
			<table style="border-spacing:0;color:#3c3c3c;font-family:-apple-system,BlinkMacSystemFont,'Neue Haas Grotesk Text Pro','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:17px">
				<tbody>
					<tr>
						<td style="background:#ffffff;border-radius:5px;padding:51px;padding-bottom:47px;padding-top:49px">
							<img src="https://lh3.googleusercontent.com/LkPpC46rNGyrCN2GrdDwnJThtZbQ43wsSRXohD19Vm_sXl-8ectJCP2WAAqp8TEbil6fPgnmwyiuFffpxV2Epmwb3LnZCXdb64YBjoTD-px2EjVhXnvuPxZizbMVYdCmiLuq2I5-_Q=w2400" height="85" width="70" style="border:0" class="CToWUd">

							<?php
							if ($type == 'register') {
								echo '<p style="line-height:27px;margin:1em 0">Were happy youre here. Click the button below to activate your account.</p>';
							} else {
								echo '<p style="line-height:27px;margin:1em 0">Klik untuk mengubah password.</p>';
							}
							?>

							<p style="line-height:27px;margin:29px 0">

								<?php if ($user == "user") : ?>
									<?php if ($type == "register") : ?>
										<a href="<?= base_url() . 'auth/verify?email=' . $email . '&token=' . urlencode($token) . '&type=register' ?>" style="background-color:#42b3bd;border:0;border-radius:100px;color:#ffffff;display:inline-block;font-size:18px;font-weight:bold;line-height:52px;outline:none;padding:0 42px;text-align:center;text-decoration:none;word-break:break-word" target="_blank">Activate your account</a>
									<?php else : ?>
										<a href="<?= base_url() . 'auth/verify?email=' . $email . '&token=' . urlencode($token) . '&type=forgotpassword' ?>" style="background-color:#42b3bd;border:0;border-radius:100px;color:#ffffff;display:inline-block;font-size:18px;font-weight:bold;line-height:52px;outline:none;padding:0 42px;text-align:center;text-decoration:none;word-break:break-word" target="_blank">Klik</a>
									<?php endif ?>
								<?php else : ?>
									<a href="<?= base_url() . 'auth/verify?email=' . $email . '&token=' . urlencode($token) . '&type=register' ?>" style="background-color:#42b3bd;border:0;border-radius:100px;color:#ffffff;display:inline-block;font-size:18px;font-weight:bold;line-height:52px;outline:none;padding:0 42px;text-align:center;text-decoration:none;word-break:break-word" target="_blank">Activate your account</a>
								<?php endif ?>
							</p>

							<div style="color:#839ca0;margin-top:41px">
								<p style="font-size:15px;line-height:23px;margin:1em 0">
									<span style="font-weight:bold">Tombol tidak berkerja?</span><br>
									Copy dan paste link di bawah ini ke browser anda.:<br>


									<?php if ($user == "user") : ?>
										<?php if ($type == "register") : ?>
											<a href="#m_-6131826731243270749_" rel="nofollow" style="border:0;color:#839ca0;font-weight:normal;outline:none;text-decoration:none"><?= base_url() . 'auth/verify?email=' . $email . '&token=' . urlencode($token) . '&type=register' ?></a>
										<?php elseif ($type == "forgotpassword") : ?>
											<a href="#m_-6131826731243270749_" rel="nofollow" style="border:0;color:#839ca0;font-weight:normal;outline:none;text-decoration:none"><?= base_url() . 'auth/verify?email=' . $email . '&token=' . urlencode($token) . '&type=forgotpassword' ?></a>
										<?php endif ?>
									<?php else : ?>
										<a href="#m_-6131826731243270749_" rel="nofollow" style="border:0;color:#839ca0;font-weight:normal;outline:none;text-decoration:none"><?= base_url() . 'auth/verify?email=' . $email . '&token=' . urlencode($token) . '&type=register' ?></a>
									<?php endif ?>
								</p>
							</div>
						</td>
					</tr>
					<tr>
						<td style="color:#aaaaaa;font-size:12px;padding:0;padding-bottom:43px;padding-top:30px">
						</td>
					</tr>
				</tbody>
			</table>

		</td>
		<td style="padding:0;width:26px"></td>
	</tr>
</tbody>
