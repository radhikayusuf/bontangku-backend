<div class="col-md-7">
	<div class="card mx-4">
		<?= form_open('auth/prosesubahpassword/' . $email, ['data-toggle' => 'validator', 'role' => 'form']) ?>

		<div class="card-body p-4">
			<div class="header mb-3">
				<button type="button" class="close float-right btn-login" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="text-center">
				<h3>
					<i class="fa fa-unlock fa-4x mb-4"></i>
				</h3>
				<h2>UBAH PASSWORD</h2>
			</div>
			<div class="form-group has-feedback">
				<label for="password">Password <span class="text-danger"><strong>*</strong></span></label>
				<input type="password" class="form-control" id="password" name="password" data-required-error="Password tidak boleh kosong" value="<?= set_value('password') ?>" required>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<span class="help-block with-errors"></span>
				<?php echo form_error('password', '<div class="text-danger">', '</div>'); ?>
			</div>

			<div class="form-group has-feedback">
				<label for="ulangipassword">Ulangi Password <span class="text-danger"><strong>*</strong></span></label>
				<input type="password" class="form-control" id="ulangipassword" name="ulangipassword" data-match="#password" data-match-error="Oppss, Password Tidak Sama." data-required-error="Ulangi Password tidak boleh kosong" value="<?= set_value('ulangipassword') ?>" required>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				<span class="help-block with-errors"></span>
				<?php echo form_error('ulangipassword', '<div class="text-danger">', '</div>'); ?>
			</div>

			<button class="btn btn-block btn-primary" type="submit">SUBMIT</button>
		</div>
		<?= form_close() ?>
	</div>
</div>
