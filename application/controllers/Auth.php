<?php defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    { 
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('fungsi');
        $this->load->library('form_validation');
        $this->load->model(['Admin_model' => 'admin']);
    }

    public function cek_login()
    {
        if ($this->session->userdata('login') == true) {
            $level = strtolower($this->session->userdata('level'));
            $url = $level."/dashboard";
            redirect(base_url($url));
        } 
    } 

    public function login($script = false)
    {
        $this->cek_login(); 

        $data['script'] = $script;
        $this->template->load('template_auth', 'auth', $data);
    }


    public function proseslogin()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');


		if ($this->form_validation->run() == false) {
            $this->login();
        } else {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
			$user = $this->admin->ambilSatuData(['email' => $email]);

            if ($user) {
                if(password_verify($password, $user['password'])){
                    if ($user['aktif'] == 1) {
                        $data = [
                            'login' => true,
                            'email' => $email,
                            'nama' => $user['nama'],
                            'level' => 'Admin'
                        ];

                        $this->session->set_userdata($data);

                        $flashcog = [
                            'message' => 'Selamat Datang ' . $user['nama'],
                            'title' => 'Login',
                            'type' => 'success'
                        ];
                        $this->session->set_flashdata($flashcog);
                        redirect('admin/dashboard', 'refresh');

                    } else {

                        $flashcog = [
                            'message' => 'Maaf email belum aktif.',
                            'title' => 'Login',
                            'type' => 'error'
                        ];
                        $this->session->set_flashdata($flashcog);
                        redirect(base_url('auth/login'));
                    }
                }else{
                    $flashcog = [
                        'message' => 'Password yang anda masukkan salah.',
                        'title' => 'Login',
                        'type' => 'error'
                    ];
                    $this->session->set_flashdata($flashcog);
                    redirect(base_url('auth/login'));
                }
            } else {
                $flashcog = [
                    'message' => 'Maaf email belum terdaftar.',
                    'title' => 'Login',
                    'type' => 'error'
                ];
                $this->session->set_flashdata($flashcog);
                redirect(base_url('auth/login'));
            }
        }
    }

    public function logout()
    {
		$data = ['login','email','nama','level'];
		$this->session->unset_userdata($data);

		$flashcog = [
			'message' => 'Anda berhasil logout.',
			'title' => 'Logout',
			'type' => 'success'
		];
		$this->session->set_flashdata($flashcog);
        redirect('auth/login');
    }

    public function page404()
    {
        $this->load->view('404');
    }

    public function page403()
    {
        $this->load->view('403');
    }

	public function proseslupapassword()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');

		if ($this->form_validation->run() == false) {
			$this->login('$(".btn-forgot-password").click();');
		} else {
			$email = $this->input->post('email');
			$user = $this->admin->ambilSatuData(['email' => $email]);
			$token = base64_encode(random_bytes(32));

			if($user){
				$this->admin->ubahData(['token' => $token], ['email' => $email]);

				$send = sendEmail($email, 2, false, $token, '', 'user', 'forgotpassword');
				if ($send) {
					$flashcog = [
						'message' => 'Silahkan cek email anda untuk proses lupa password.',
						'title' => 'Info Lupa Password',
						'type' => 'success'
					];
					$this->session->set_flashdata($flashcog);
				} else {
					$flashcog = [
						'message' => $send,
						'title' => 'Info Lupa Password',
						'type' => 'error'
					];
					$this->session->set_flashdata($flashcog);
				}

				redirect('auth/login');

			}else{
				$flashcog = [
					'message' => 'Maaf email belum terdaftar.',
					'title' => 'Lupa Password',
					'type' => 'error'
				];
				$this->session->set_flashdata($flashcog);
				redirect(base_url('auth/login'));
			}
		}
	}

	public function verify()
	{
		$email = $this->input->get('email');
		$token = $this->input->get('token');
		$type = $this->input->get('type');

		$user_token = $this->admin->ambilSatuData(['email' => $email]);


		if ($user_token) {
			if ($token == $user_token['token']) {

				$this->session->set_userdata(['token' => $token]);

				redirect(base_url('auth/ubahpassword/' . $email));

			} else {
				$flashcog = [
					'message' => 'Maaf token tidak valid.',
					'title' => 'Info Login',
					'type' => 'error'
				];
				$this->session->set_flashdata($flashcog);
				redirect('auth/login');
			}
		} else {
			$flashcog = [
				'message' => 'Maaf email belum terdaftar didalam sistem.',
				'title' => 'Info Login',
				'type' => 'error'
			];
			$this->session->set_flashdata($flashcog);
			redirect('auth/login');
		}
	}

	public function ubahpassword($email)
	{
		$user_token = $this->admin->ambilSatuData(['email' => $email]);
		$token = $this->session->userdata('token');
		$data['email'] = $email;

		if ($token == $user_token['token']) {

			$this->template->load('template_auth', 'ubahpassword', $data);
		} else {
			$flashcog = [
				'message' => 'Maaf token tidak valid.',
				'title' => 'Info Login',
				'type' => 'error'
			];
			$this->session->set_flashdata($flashcog);
			redirect('auth/login');
		}
	}

	public function prosesubahpassword($email)
	{
		$this->form_validation->set_rules('password', 'Password', 'required', ['required' => 'Password tidak boleh kosong']);
		$this->form_validation->set_rules('ulangipassword', 'Ulangi Password', 'required', ['required' => 'Ulangi Password tidak boleh kosong']);


		if ($this->form_validation->run() == false) {
			$this->ubahpassword($email);
		} else {
			$password = $this->input->post('password');

			$data = [
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
			];


			$this->admin->ubahData($data, ['email' => $email]);
			$flashcog = [
				'message' => 'Password berhasil diubah, silahkan untuk login kembali.',
				'title' => 'Lupa password.',
				'type' => 'success'
			];
			$this->session->set_flashdata($flashcog);
			redirect(base_url('auth/login'));
		}
	}

}
