<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
	public $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['ModelMahasiswa' => 'mahasiswa', 'ModelPaslon', 'Main_model']);
		$this->cek_cookie();
	}

	public function cek_cookie()
	{
		if (get_cookie('login_mahasiswa')) {
			$nim = get_cookie('login_mahasiswa');

			$user = $this->mahasiswa->getWhereDataMahasiswa(['nim' => $nim])->row_array();
			$data = [
				'login_mahasiswa' => true,
				'nim' => $nim,
				'nama' => $user['nama']
			];

			$this->session->set_userdata($data);
		}
	}

	public function cek_login()
	{
		if ($this->session->userdata('login_mahasiswa') == true) {
			redirect(base_url());
		}
	}
	

	public function index()
	{
		$this->data['jumlah_mahasiswa'] = $this->Main_model->getWhereData('mahasiswa', ['tahun' => date('Y')])->num_rows();
		$this->data['jumlah_paslon'] = $this->Main_model->getWhereData('paslon', ['tahun' => date('Y')])->num_rows();
		$this->data['jumlah_telah_memilih'] = $this->Main_model->getWhereData('voting', ['tahun' => date('Y')])->num_rows();

		$this->template->load('template_user', 'mahasiswa/index', $this->data);
	}

	public function quickcount()
	{
		$this->data['paslon'] = $this->ModelPaslon->getAllDataPaslon();

		$where = ['tahun' => date('Y')];
		$this->data['jumlah_vote'] = $this->Main_model->getWhereData('voting', $where)->num_rows();

		$this->template->load('template_user', 'mahasiswa/menu/quickcount', $this->data);
	}

	public function kandidat()
	{
		$this->data['paslon'] = $this->ModelPaslon->getAllDataPaslon();
		$this->template->load('template_user', 'mahasiswa/menu/kandidat', $this->data);
	}

	public function voting()
	{
		$this->data['paslon'] = $this->ModelPaslon->getAllDataPaslon();

		$where = [
			'id_mahasiswa' => $this->session->userdata('id_mahasiswa'),
			'tahun' => date('Y')
		];
		$this->data['isvoted'] = $this->mahasiswa->getMahasiswaVote($where)->num_rows();
		$this->data['voting'] = $this->mahasiswa->getMahasiswaVote($where)->row_array();

		$this->template->load('template_user', 'mahasiswa/menu/voting', $this->data);
	}

	public function login()
	{
		$this->cek_login();
		$this->load->view('mahasiswa/template/header');
		$this->load->view('mahasiswa/menu/login');
		$this->load->view('mahasiswa/template/footer-link');
	}

	public function register()
	{
		$this->cek_login();
		$this->load->view('mahasiswa/template/header');
		$this->load->view('mahasiswa/menu/register');
		$this->load->view('mahasiswa/template/footer-link');
	}

	public function visitor()
	{
		$ip    = $this->input->ip_address(); // Mendapatkan IP user
		$date  = date("Y-m-d"); // Mendapatkan tanggal sekarang
		$waktu = time(); //
		$timeinsert = date("Y-m-d H:i:s");

		// Cek berdasarkan IP, apakah user sudah pernah mengakses hari ini
		$s = $this->db->query("SELECT * FROM visitor WHERE ip='" . $ip . "' AND date='" . $date . "'")->num_rows();
		$ss = isset($s) ? ($s) : 0;


		// Kalau belum ada, simpan data user tersebut ke database
		if (
			$ss == 0
		) {
			$this->db->query("INSERT INTO visitor(ip, date, hits, online, time) VALUES('" . $ip . "','" . $date . "','1','" . $waktu . "','" . $timeinsert . "')");
		}

		// Jika sudah ada, update
		else {
			$this->db->query("UPDATE visitor SET hits=hits+1, online='" . $waktu . "' WHERE ip='" . $ip . "' AND date='" . $date . "'");
		}


		$pengunjunghariini  = $this->db->query("SELECT * FROM visitor WHERE date='" . $date . "' GROUP BY ip")->num_rows(); // Hitung jumlah pengunjung

		$dbpengunjung = $this->db->query("SELECT COUNT(hits) as hits FROM visitor")->row();

		$totalpengunjung = isset($dbpengunjung->hits) ? ($dbpengunjung->hits) : 0; // hitung total pengunjung

		$bataswaktu = time() - 300;

		$pengunjungonline  = $this->db->query("SELECT * FROM visitor WHERE online > '" . $bataswaktu . "'")->num_rows(); // hitung pengunjung online


		$data['pengunjunghariini'] = $pengunjunghariini;
		$data['totalpengunjung'] = $totalpengunjung;
		$data['pengunjungonline'] = $pengunjungonline;

		return $data;
	}
}
