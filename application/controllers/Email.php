 <?php defined('BASEPATH') or exit('No direct script access allowed');
    class Email extends CI_Controller
    {
        public function send()
        {
			// $config = [
			// 	'protocol' => 'smtp',
			// 	'smtp_host' => 'mail.aspacud.org',
			// 	'smtp_user' => 'rifqi@aspacud.org',
			// 	'smtp_pass' => 'rifqi123',
			// 	'smtp_port' => 587,
			// 	'mailtype' => 'html',
			// 	'charset' => 'utf-8',
			// 	'newline' => "\r\n"
			// ];



			$config = array(
				'protocol' => 'smtp',
				'smtp_host' => 'smtp.mailtrap.io',
				'smtp_port' => 2525,
				'smtp_user' => '430b3cf6903a9e',
				'smtp_pass' => '31aa5340c222a9',
				'crlf' => "\r\n",
				'newline' => "\r\n"
			);

			$config['smtp_timeout'] = '7';
			$config['validation'] = FALSE;

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from('info@aspacud.org', 'APUD');
            $this->email->to('ramdhanirifqi8@gmail.com');
            $this->email->subject('Percobaan email');

            $data = [
                'email' => 'ramdhanirifqi8@gmail.com',
                'token' => base64_encode(random_bytes(33)),
                'password' => 'rifqi123',
                'type' => 'forgotpassword',
                'user' => 'user',
                'pesan' => 'hallo aku siapa'
            ];

            $body = $this->load->view('template_email2', $data, TRUE);
            $this->email->message($body); 
            if (!$this->email->send()) {
                echo $this->email->print_debugger();
            } else {
                echo 'Success to send email';
            }
        }

		public function send_mail_action()
		{

			$this->data['Email_Name'] = "Rifqi Ramdhani";

			$message = $this->load->view('eformgis_table/email_body', '', TRUE);

			$configArr = array(
				'subject' => "APUD GIS Webtraining Zoom Invitation 21-02-2021",
				'email_to' => 'ramdhanirifqi8@gmail.com',
				'message' => $message,
				'from_name' => "rifqi@aspacud.org",
				'from_email' => "rifqi@aspacud.org"
			);

			$this->SendMail($configArr);
			
		}

		public function SendMail($configArr)
		{
			$resultMail = FALSE;


			if (!empty($configArr)) {

				$mailto = $configArr['email_to'];
				$subject = $configArr['subject'];
				$message = $configArr['message'];

				// $content = file_get_contents($file);
				// $content = chunk_split(base64_encode($content));

				// a random hash will be necessary to send mixed content
				// $separator = md5(time());

				// carriage return type (RFC)
				$eol = "\r\n";

				// main header (multipart mandatory)
				$headers = "From: " . $configArr['from_name'] . " <" . $configArr['from_email'] . ">" . $eol;
				$headers .= "Cc: " . $configArr['from_email'] . "\r\n";
				$headers .= "MIME-Version: 1.0" . $eol;
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				// $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"" . $eol;
				// $headers .= "Content-Transfer-Encoding: 7bit" . $eol;
				// $headers .= "This is a MIME encoded message." . $eol;

				// message
				// $body = "--" . $separator . $eol;
				$body = "Content-Type: text/html; charset=\"iso-8859-1\"" . $eol;
				$body .= "Content-Transfer-Encoding: 8bit" . $eol;
				$body = $message;
				$body .= $message . $eol;

				//SEND Mail
				if (mail($mailto, $subject, $body, $headers)) {
					$resultMail = TRUE;
				} else {
					$resultMail = FALSE;
				}
			}

			return $resultMail;
		}
    }
