<?php defined('BASEPATH') or exit('No direct script access allowed');

class ManajemenLokasi extends CI_Controller
{
	public $level;
	public $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('fungsi');
		$this->load->model(['Lokasi_model' => 'lokasi']);
		//
		// $this->cek_login();
	}

	public function cek_login()
	{
		$level = 'Admin';
		if ($this->session->userdata('level') != $level || empty($this->session->userdata('login'))) {
			redirect(base_url('auth/login'));
		}
	}

	public function index()
	{
		$this->data['level'] = strtolower($this->session->userdata('level'));
		$this->data['title'] = "Manajemen Lokasi";

		$this->data['lokasi'] = $this->lokasi->ambilSemuaData();

		$this->template->load('template', 'admin/manajemenlokasi/index', $this->data);
	}

	public function tambah()
	{
		$this->data['title'] = "Tambah Lokasi";
		$this->template->load('template', 'admin/manajemenlokasi/tambah', $this->data);
	}

	public function ubah($id)
	{
		$this->data['lokasi'] = $this->lokasi->ambilSatuData(['id_lokasi' => $id]);
		$this->data['title'] = "Ubah Lokasi";
		$this->data['id_lokasi'] = $id;
		$this->template->load('template', 'admin/manajemenlokasi/ubah', $this->data);
	}

	public function prosestambahlokasi()
	{
		// echo '<pre>';
		// var_dump($this->input->post());
		// // var_dump($_FILES);
		// echo '</pre>';
		// die;

		$this->form_validation->set_rules('nama_lokasi', 'Nama Lokasi', 'required', ['required' => 'Nama Lokasi tidak boleh kosong']);
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required', ['required' => 'Deskripsi tidak boleh kosong']);
		$this->form_validation->set_rules('type', 'Type', 'required', ['required' => 'Type tidak boleh kosong']);
		$this->form_validation->set_rules('url', 'Url', 'required', ['required' => 'Url tidak boleh kosong']);
		$this->form_validation->set_rules('latitude', 'Latitude', 'required', ['required' => 'Latitude tidak boleh kosong']);
		$this->form_validation->set_rules('longtitude', 'Longtitude', 'required', ['required' => 'Longtitude tidak boleh kosong']);

		if ($this->form_validation->run() == false) {
			$this->tambah();
		} else {
			$data = [
				'nama_lokasi' => $this->input->post('nama_lokasi'),
				'deskripsi' => $this->input->post('deskripsi'),
				'type' => $this->input->post('type'),
				'url' => $this->input->post('url'),
				'latitude' => $this->input->post('latitude'),
				'longtitude' => $this->input->post('longtitude'),
			];

			$this->lokasi->tambahData($data);
			$flashcog = [
				'message' => 'Data lokasi berhasil ditambahkan.',
				'title' => 'Manajemen Lokasi',
				'type' => 'success'
			];
			$this->session->set_flashdata($flashcog);
			redirect(base_url('admin/manajemenlokasi'));
		}
	}

	public function prosesubahlokasi($id)
	{
		// echo '<pre>';
		// var_dump($this->input->post());
		// var_dump($_FILES);
		// echo '</pre>';


		$this->form_validation->set_rules('nama_lokasi', 'Nama Lokasi', 'required', ['required' => 'Nama Lokasi tidak boleh kosong']);
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required', ['required' => 'Deskripsi tidak boleh kosong']);
		$this->form_validation->set_rules('type', 'Type', 'required', ['required' => 'Type tidak boleh kosong']);
		$this->form_validation->set_rules('url', 'Url', 'required', ['required' => 'Url tidak boleh kosong']);
		$this->form_validation->set_rules('latitude', 'Latitude', 'required', ['required' => 'Latitude tidak boleh kosong']);
		$this->form_validation->set_rules('longtitude', 'Longtitude', 'required', ['required' => 'Longtitude tidak boleh kosong']);

		if ($this->form_validation->run() == false) {
			$this->ubah($id);
		} else {
			$data = [
				'nama_lokasi' => $this->input->post('nama_lokasi'),
				'deskripsi' => $this->input->post('deskripsi'),
				'type' => $this->input->post('type'),
				'url' => $this->input->post('url'),
				'latitude' => $this->input->post('latitude'),
				'longtitude' => $this->input->post('longtitude'),
			];



			$this->lokasi->ubahData($data, ['id_lokasi' => $id]);
			$flashcog = [
				'message' => 'Data lokasi berhasil diubah.',
				'title' => 'Manajemen Lokasi',
				'type' => 'success'
			];
			$this->session->set_flashdata($flashcog);
			redirect(base_url('admin/manajemenlokasi'));
		}
	}

	public function proseshapuslokasi($id)
	{
		$this->lokasi->deleteData(['id_lokasi' => $id]);

		if ($this->db->error()) {
			echo $this->db->error()['message'];
		}
	}

	public function json()
	{
		$data_lokasi = $this->lokasi->ambilSemuaData();

		$data_json = [];
		foreach ($data_lokasi as $getlokasi) {
			$data['latitude'] = $getlokasi->latitude;
			$data['longtitude'] = $getlokasi->longtitude;

			array_push($data_json, $data);
		}

		echo json_encode($data_json);
	}
}
