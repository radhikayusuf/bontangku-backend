<?php defined('BASEPATH') or exit('No direct script access allowed');

class ManajemenUser extends CI_Controller
{
	public $level;
	public $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('fungsi');
		$this->load->model(['User_model' => 'user']);
		//
		// $this->cek_login();
	}

	public function cek_login()
	{
		$level = 'Admin';
		if ($this->session->userdata('level') != $level || empty($this->session->userdata('login'))) {
			redirect(base_url('auth/login'));
		}
	}

	public function index()
	{
		$this->data['level'] = strtolower($this->session->userdata('level'));
		$this->data['title'] = "Manajemen User";

		$this->data['user'] = $this->user->ambilSemuaData();

		$this->template->load('template', 'admin/manajemenuser/index', $this->data);
	}

	public function tambah()
	{
		$this->data['title'] = "Tambah User";
		$this->template->load('template', 'admin/manajemenuser/tambah', $this->data);
	}

	public function ubah($email)
	{
		$this->data['user'] = $this->user->ambilSatuData(['email' => $email]);
		$this->data['title'] = "Ubah User";
		$this->data['email'] = $email;
		$this->template->load('template', 'admin/manajemenuser/ubah', $this->data);
	}

	public function prosestambahuser()
	{
		// echo '<pre>';
		// var_dump($this->input->post());
		// var_dump($_FILES);
		// echo '</pre>';


		$this->form_validation->set_rules('nama', 'Nama', 'required', ['required' => 'Nama tidak boleh kosong']);
		$this->form_validation->set_rules('no_hp', 'No HP', 'required|is_unique[user.no_hp]', ['is_unique' => 'Maaf No HP telah digunakan','required' => 'No HP tidak boleh kosong']);
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]', ['is_unique' => 'Maaf email telah digunakan', 'required' => 'Email tidak boleh kosong']);
		$this->form_validation->set_rules('password', 'Password','required', ['required' => 'Password tidak boleh kosong']);
		$this->form_validation->set_rules('ulangipassword', 'Ulangi Password','required', ['required' => 'Ulangi Password tidak boleh kosong']);

		if ($this->form_validation->run() == false) {
			$this->tambah();
		} else {
			$data = [
				'nama' => ucwords($this->input->post('nama')),
				'email' => $this->input->post('email'),
				'no_hp' => $this->input->post('no_hp'),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
			];

			$this->user->tambahData($data);
			$flashcog = [
				'message' => 'Data user berhasil ditambahkan.',
				'title' => 'Manajemen User',
				'type' => 'success'
			];
			$this->session->set_flashdata($flashcog);
			redirect(base_url('admin/manajemenuser'));
		}
	}

	public function prosesubahuser($email)
	{
		// echo '<pre>';
		// var_dump($this->input->post());
		// var_dump($_FILES);
		// echo '</pre>';


		$this->form_validation->set_rules('nama', 'Nama', 'required', ['required' => 'Nama tidak boleh kosong']);
		$this->form_validation->set_rules('no_hp', 'No HP', 'required', ['required' => 'No HP tidak boleh kosong']);
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email', ['required' => 'Email tidak boleh kosong']);

		if ($this->form_validation->run() == false) {
			$this->ubah($email);
		} else {

			if(empty($password)){
				$data = [
					'nama' => ucwords($this->input->post('nama')),
					'email' => $this->input->post('email'),
					'no_hp' => $this->input->post('no_hp'),
				];
			}else{
				$data = [
					'nama' => ucwords($this->input->post('nama')),
					'email' => $this->input->post('email'),
					'no_hp' => $this->input->post('no_hp'),
					'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
				];
			}
			

			$this->user->ubahData($data, ['email' => $email]);
			$flashcog = [
				'message' => 'Data user berhasil diubah.',
				'title' => 'Manajemen User',
				'type' => 'success'
			];
			$this->session->set_flashdata($flashcog);
			redirect(base_url('admin/manajemenuser'));
		}
	}

	public function proseshapususer($id)
	{
		$this->user->deleteData(['id_user' => $id]);

		if ($this->db->error()) {
			echo $this->db->error()['message'];
		}
	}
}
