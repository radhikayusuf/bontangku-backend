<?php defined('BASEPATH') or exit('No direct script access allowed');

class Layanan extends CI_Controller
{
	public $level;
	public $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('fungsi');
		$this->load->model(['Main_model' => 'main']);
		//
		// $this->cek_login();
	}

	public function cek_login()
	{
		$level = 'Admin';
		if ($this->session->userdata('level') != $level || empty($this->session->userdata('login'))) {
			redirect(base_url('auth/login'));
		}
	}

	public function index()
	{
		$this->data['level'] = strtolower($this->session->userdata('level'));
		$this->data['title'] = "Layanan 112";

		$this->data['layanan'] = $this->db->join('user', 'layanan.id_user = user.id_user')->get('layanan')->result();

		$this->template->load('template', 'admin/layanan/index', $this->data);
	}

}
