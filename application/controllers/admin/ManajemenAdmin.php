<?php defined('BASEPATH') or exit('No direct script access allowed');

class ManajemenAdmin extends CI_Controller
{
	public $level;
	public $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('fungsi');
		$this->load->model(['Admin_model' => 'admin']);
		//
		// $this->cek_login();
	}

	public function cek_login()
	{
		$level = 'Admin';
		if ($this->session->userdata('level') != $level || empty($this->session->userdata('login'))) {
			redirect(base_url('auth/login'));
		}
	}

	public function index()
	{
		$this->data['level'] = strtolower($this->session->userdata('level'));
		$this->data['title'] = "Manajemen Admin";

		$this->data['admin'] = $this->admin->ambilSemuaData();

		$this->template->load('template', 'admin/manajemenadmin/index', $this->data);
	}

	public function tambah()
	{
		$this->data['title'] = "Tambah Admin";
		$this->template->load('template', 'admin/manajemenadmin/tambah', $this->data);
	}

	public function ubah($email)
	{
		$this->data['admin'] = $this->admin->ambilSatuData(['email' => $email]);
		$this->data['title'] = "Ubah Admin";
		$this->data['email'] = $email;
		$this->template->load('template', 'admin/manajemenadmin/ubah', $this->data);
	}

	public function prosestambahadmin()
	{
		// echo '<pre>';
		// var_dump($this->input->post());
		// var_dump($_FILES);
		// echo '</pre>';


		$this->form_validation->set_rules('nama', 'Nama','required', ['required' => 'Nama tidak boleh kosong']);
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[admin.email]', ['is_unique' => 'Maaf email telah digunakan', 'required' => 'Email tidak boleh kosong']);
		$this->form_validation->set_rules('password', 'Password','required', ['required' => 'Password tidak boleh kosong']);
		$this->form_validation->set_rules('ulangipassword', 'Ulangi Password','required', ['required' => 'Ulangi Password tidak boleh kosong']);

		if ($this->form_validation->run() == false) {
			$this->tambah();
		} else {
			$data = [
				'nama' => ucwords($this->input->post('nama')),
				'email' => $this->input->post('email'),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
			];

			$this->admin->tambahData($data);
			$flashcog = [
				'message' => 'Data admin berhasil ditambahkan.',
				'title' => 'Manajemen Admin',
				'type' => 'success'
			];
			$this->session->set_flashdata($flashcog);
			redirect(base_url('admin/manajemenadmin'));
		}
	}

	public function prosesubahadmin($email)
	{
		// echo '<pre>';
		// var_dump($this->input->post());
		// var_dump($_FILES);
		// echo '</pre>';


		$this->form_validation->set_rules('nama', 'Nama', 'required', ['required' => 'Nama tidak boleh kosong']);
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email', ['required' => 'Email tidak boleh kosong']);

		if ($this->form_validation->run() == false) {
			$this->ubah($email);
		} else {
			$password = $this->input->post('password');

			if(empty($password)){
				$data = [
					'nama' => ucwords($this->input->post('nama')),
					'email' => $this->input->post('email')
				];
			}else{
				$data = [
					'nama' => ucwords($this->input->post('nama')),
					'email' => $this->input->post('email'),
					'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT)
				];
			}


			$this->admin->ubahData($data, ['email' => $email]);
			$flashcog = [
				'message' => 'Data admin berhasil diubah.',
				'title' => 'Manajemen Admin',
				'type' => 'success'
			];
			$this->session->set_flashdata($flashcog);
			redirect(base_url('admin/manajemenadmin'));
		}
	}

	public function prosesHapusAdmin($id)
	{
		$this->admin->deleteData(['id' => $id]);

		if ($this->db->error()) {
			echo $this->db->error()['message'];
		}
	}
}
