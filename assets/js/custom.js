//function swall alert hapus data
function hapusdata(title, url, id, nama) {
    Swal.fire({
        title: 'Apakah anda yakin?',
        text: "Ingin Menghapus Data " + nama,
        icon: 'warning',
        showCancelButton: true,
		confirmButtonColor: '#1C42CA',
		cancelButtonColor: '#f86c6b',
        confirmButtonText: "Hapus",
        cancelButtonText: "Kembali",
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: url + id,
                type: 'GET',
                error: function (xhr, textStatus, error) {
                    console.log(xhr.responseText);
                    console.log(xhr.statusText);
                    // console.log(textStatus);
                    // console.log(error);
                    Swal.fire({
                        title: title,
                        text: 'Internal Server Error, Check Console For Detail Information',
                        icon: 'error'
                    })
                },
                success: function (data) {
                    $("#" + id).remove();
                    Swal.fire({
                        title: title,
                        text: 'Data Berhasil Dihapus!',
                        icon: 'success'
                    }) 
                }
            });
        }
    })
}

function swalhref(link, titletext) {
    Swal.fire({
        title: 'Apakah anda yakin',
        text: " " + titletext,
        icon: 'warning',
        showCancelButton: true,
		confirmButtonColor: '#1C42CA',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
    }).then((result) => {
        if (result.value) {
            window.location.href = link
        }
    })
}

function swalconfirm(form, titletext) {
	Swal.fire({
		title: 'Warning!',
		text: " " + titletext,
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#1C42CA',
		confirmButtonText: 'Submit',
		cancelButtonText: 'Kembali'
	}).then((isConfirm) => {
		if (isConfirm) form.submit()
	})
}

